package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
// This Application will function as an endpoint that will be used in handling http request.
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam"	annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return  String.format("Hi %s", name);
	}

//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/{name}
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

//	Activity S09 Starts HERE!

//	Create ArrayList
	ArrayList<String> list = new ArrayList<String>();
// Route for Enroll
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user){
		if(list.add(user)){
			return String.format("Thank you for enrolling, %s!", user);
		}else {
			return String.format("UnSuccessful Enrollment!");
		}
	}
// Get Enrollees from the list array
	@GetMapping("/getEnrollees")
	public String enrollees(){
		return list.toString();
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name") String name, @RequestParam(value = "age") Integer age){
		return String.format("Hello %s! My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courseId(@PathVariable("id") String courseId){
		if(courseId.equals("java101")){
			return "Java 101, MWF 8:00AM-11:00AM, PHP 3000.00";
		}else if (courseId.equals("sql101")){
			return "SQL, TTH 1:00PM-4:00PM, PHP 2000.00";
		}else if(courseId.equals("javaee101")){
			return "Java EE 101, MWF 1:00PM-4:00PM, PHP 3500.00";
		}else{
			return "Course is not Found!";
		}
	}
}
